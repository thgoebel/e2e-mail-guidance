# An Internet Draft of Guidance for E-mail Crypto Implementers

This repository hosts the source for [draft-ietf-lamps-e2e-mail-guidance](https://datatracker.ietf.org/doc/draft-ietf-lamps-e2e-mail-guidance/).
