---
title: Guidance on End-to-End E-mail Security
docname: draft-ietf-lamps-e2e-mail-guidance-17
category: info

area: int
workgroup: lamps
keyword: Internet-Draft
submissionType: IETF
v: 3

venue:
  group: "LAMPS"
  type: "Working Group"
  mail: "spasm@ietf.org"
  arch: "https://mailarchive.ietf.org/arch/browse/spasm/"
  repo: "https://gitlab.com/dkg/e2e-mail-guidance"
  latest: "https://dkg.gitlab.io/e2e-mail-guidance/"

author:
  -
    ins: D. K. Gillmor
    name: Daniel Kahn Gillmor
    org: American Civil Liberties Union
    street: 125 Broad St.
    city: New York, NY
    code: 10004
    country: USA
    abbrev: ACLU
    email: dkg@fifthhorseman.net
    role: editor
  -
    ins: B. Hoeneisen
    name: Bernie Hoeneisen
    org: pEp Project
    street: Oberer Graben 4
    city: 8400 Winterthur
    country: Switzerland
    email: bernie.hoeneisen@pep-project.org
    uri: https://pep-project.org/
    role: editor
  -
    ins: A. Melnikov
    name: Alexey Melnikov
    org: Isode Ltd
    street: 14 Castle Mews
    city: Hampton, Middlesex
    code: TW12 2NP
    country: UK
    email: alexey.melnikov@isode.com
    role: editor
informative:
  AUTOCRYPT:
    target: https://autocrypt.org/
    title: Autocrypt - Convenient End-to-End Encryption for E-Mail
    date: July 2018
    author:
     -
      name: Vincent Breitmoser
     -
      name: Holger Krekel
     -
      name: Daniel Kahn Gillmor
      ins: D. K. Gillmor
  chrome-indicators:
    target: https://blog.chromium.org/2018/05/evolving-chromes-security-indicators.html
    title: Evolving Chrome's security indicators
    author:
      name: Emily Schechter
    date: May 2018
  EFAIL:
    target: https://efail.de
    date: August 2018
    title: "Efail: breaking S/MIME and OpenPGP email encryption using exfiltration channels"
    author:
      -
        name: Damian Poddebniak
      -
        name: Christian Dresen
      -
        name: Jens Müller
      -
        name: Fabian Ising
      -
        name: Sebastian Schinzel
      -
        name: Simon Friedberger
      -
       name: Juraj Somorovsky
      -
        name: Jörg Schwenk
  mixed-content:
    target: https://www.w3.org/TR/mixed-content/
    date: February 2023
    title: Mixed Content
  ORACLE:
    target: https://www.usenix.org/conference/usenixsecurity23/presentation/ising
    title: "Content-Type: multipart/oracle Tapping into Format Oracles in Email End-to-End Encryption"
    date: August 2023
    author:
      -
        name: Fabian Ising
      -
        name: Damian Poddebniak
      -
        name: Tobias Kappert
      -
        name: Christoph Saatjohann
      -
        name: Sebastian Schinzel
  SRI:
    title: Subresource Integrity
    target: https://www.w3.org/TR/SRI/
    date: June 2016
  PKCS11:
    title: PKCS #11 Specification Version 3.1
    author:
     -
       name: Dieter Bong
     -
       name: Tony Cox
    date: 2023-07-23
    target: https://docs.oasis-open.org/pkcs11/pkcs11-spec/v3.1/os/pkcs11-spec-v3.1-os.html
  DROWN:
    title: "DROWN: Breaking TLS using SSLv2"
    target: https://drownattack.com/
    date: March 2016
    author:
      -
        name: Nimrod Aviram
      -
        name: Sebastian Schinzel
      -
        name: Juraj Somorovsky
      -
        name: Nadia Heninger
      -
        name: Maik Dankel
      -
        name: Jens Steube
      -
        name: Luke Valenta
      -
        name: David Adrian
      -
        name: J. Alex Halderman
      -
        name: Viktor Dukhovni
      -
        name: Emilia Käsper
      -
        name: Shaanan Cohney
      -
        name: Susanne Engels
      -
        name: Christof Paar
      -
        name: Yuval Shavitt
  IKE:
    title: "The Dangers of Key Reuse: Practical Attacks on IPsec IKE"
    target: https://www.usenix.org/system/files/conference/usenixsecurity18/sec18-felsch.pdf
    date: August 2018
    author:
      -
        name: Dennis Felsch
      -
        name: Martin Grothe
      -
        name: Jörg Schwenk
      -
        name: Adam Czubak
      -
        name: Marcin Szymane
  REPLY: DOI.10.1007/978-3-030-21568-2_2
  SPOOFING:
    title: "“Johnny, you are fired!” – Spoofing OpenPGP and S/MIME Signatures in Emails"
    target: https://www.usenix.org/system/files/sec19-muller.pdf
    author:
      -
        name: Jens Müller
      -
        name: Marcus Brinkmann
      -
        name: Damian Poddebniak
      -
        name: Hanno Böck
      -
        name: Sebastian Schinzel
      -
        name: Juraj Somorovsky
      -
        name: Jörg Schwenk
    date: August 2019
--- abstract

End-to-end cryptographic protections for e-mail messages can provide useful security.
However, the standards for providing cryptographic protection are extremely flexible.
That flexibility can trap users and cause surprising failures.
This document offers guidance for mail user agent implementers to help mitigate those risks, and to make end-to-end e-mail simple and secure for the end user.
It provides a useful set of vocabulary as well as recommendations to avoid common failures.
It also identifies a number of currently unsolved usability and interoperability problems.

--- middle

# Introduction

E-mail end-to-end security using S/MIME ({{!RFC8551}}) and PGP/MIME ({{!RFC3156}}) cryptographic standards can provide integrity, authentication and confidentiality to MIME ({{!RFC4289}}) e-mail messages.

However, there are many ways that a receiving mail user agent can misinterpret or accidentally break these security guarantees.
For example, [EFAIL]'s "Direct Exfiltration" attack leaks cleartext due to an attack that splices existing ciphertext into a new message, which is then handled optimistically (and wrongly) by many mail user agents.

A mail user agent that interprets a message with end-to-end cryptographic protections needs to do so defensively, staying alert to different ways that these protections can be bypassed by mangling (either malicious or accidental) or a failed user experience.

A mail user agent that generates a message with end-to-end cryptographic protections should be aware of these defensive interpretation strategies, and should compose any new outbound message conservatively if they want the protections to remain intact.

This document offers guidance to the implementer of a mail user agent that provides these cryptographic protections, whether for sending or receiving mail.
An implementation that follows this guidance will provide its users with stronger and easier-to-understand security properties, and will also offer more reliable interoperability for messages exchanged with other implementations.

In {{future-work}}, this document also identifies a number of interoperability and usability concerns for end-to-end cryptographic e-mail which have no current broadly accepted technical standard for resolution.
One major area not covered in this document is the acquisition and long-term maintenance of cryptographic identity information and metadata across multiple mail user agents controlled by the same user.

{::boilerplate bcp14-tagged}

## Terminology

For the purposes of this document, we define the following concepts:

- *MUA* is short for Mail User Agent; an e-mail client.
- *Protection* of message data refers to cryptographic encryption and/or signatures, providing confidentiality, authenticity, and/or integrity.
- *Cryptographic Layer*, *Cryptographic Envelope*, *Cryptographic Payload*, *Cryptographic Summary*, and *Errant Cryptographic Layer* are defined in {{cryptographic-structure}}
- A *well-formed* e-mail message with cryptographic protection has both a *Cryptographic Envelope* and a *Cryptographic Payload*.
- *Structural Header Fields* are documented in {{structural-header-fields}}.
- *User-Facing Header Fields* are documented in {{user-facing-header-fields}}.
- *Main Body Part* is the part (or parts) that are typically rendered to the user as the message itself (not "as an attachment").  See {{main-body-part}}.

This document contains extensive discussion about end-to-end cryptographic protections in e-mail, while acknowledging that many MUAs have no capabilities for end-to-end cryptographic protections at all.
We divide MUAs into three distinct profiles:

- A *Non-cryptographic* MUA has no capabilities for end-to-end cryptographic protections.
- A *Conformant* MUA follows the guidance in this document when dealing with end-to-end cryptographic protections.
- A *Legacy* MUA has capabilities for end-to-end cryptographic protections, but does not adhere to the the guidance in this document.

At the time of the writing of this document, most MUAs with cryptographic protections are legacy MUAs.

### Structural Header Fields {#structural-header-fields}

A message header field named `MIME-Version`, or whose name begins with `Content-` is referred to in this document as a "structural" header field.
This is a less-ambiguous name for what {{?RFC2045}} calls "MIME Header Fields".

These header fields indicate something about the specific MIME part they are attached to, and cannot be transferred or copied to other parts without endangering the readability of the message.

This includes:

- `MIME-Version`
- `Content-Type`
- `Content-Transfer-Encoding`
- `Content-Disposition`

### User-Facing Header Fields {#user-facing-header-fields}

Of all the header fields that an e-mail message may contain, only a handful are typically presented directly to the user.
This document refers to them as "user-facing" header fields.
Typically, user-facing header fields are:

 - `Subject`
 - `From`
 - `To`
 - `Cc`
 - `Date`
 - `Reply-To`
 - `Followup-To`
 - `Sender`
 - `Resent-From`
 - `Resent-To`
 - `Resent-Cc`
 - `Resent-Date`
 - `Resent-Sender`

The above list are the header fields most often presented directly to the user who views a message, though an MUA may also decide to treat any other header field as "user-facing".
Of course, many of these header fields are entirely absent from any given message, and an absent header field is not presented to the user at all.

Note that the resending header fields (those beginning with `Resent-`) are typically only added by an intervening MUA (see {{Section 3.6.6 of RFC5322}} and {{intervening-mua}} of this document).
As such, though they may in some cases be presented to the user, they will typically not bear any end-to-end cryptographic protection (even if the original header fields of a message are protected, see {{message-header-fields}}), because they are unknown to the original sender.

Other header fields may affect the visible rendering of the message (e.g., `References` and `In-Reply-To` may affect the placement of a message in a threaded discussion; or the `List-*` and `Archived-At` header fields added by mailing lists may cause additional buttons to be displayed during rendering), but they are not directly displayed to the user and so are not considered "user-facing".

# Usability

Any MUA that enables its user to transition from unprotected messages to messages with end-to-end cryptographic protection needs to consider how the user understands this transition.
That said, the primary goal of the user of an MUA is communication -- so interface elements that interfere with communication should be avoided where possible.

Furthermore, it is a near certainty that the user will continue to encounter unprotected messages, and may need to send unprotected messages (for example, if a given recipient cannot handle cryptographic protections).
This means that the MUA needs to provide the user with some guidance, so that they understand what protections any given message or conversation has.
But the user should not be overwhelmed with choices or presented with unactionable information.

## Simplicity

The end user (the operator of the MUA) is unlikely to understand complex end-to-end cryptographic protections on any e-mail message, so keep it simple.

For clarity to the user, any cryptographic protections should apply to the message as a whole, not just to some subparts.

This is true for message composition: the standard message composition user interface of an MUA should offer minimal controls which indicate which types of protection to apply to the new message as a whole.

This is also true for message interpretation: the standard message rendering user interface of an MUA should offer a minimal, clear indicator about the end-to-end cryptographic status of the message as a whole.

See {{types-of-protection}} for more detail about mental models and cryptographic status.

(It is of course possible that a message forwarded as a MIME attachment could have its own cryptographic status while still being a message subpart; but that status should be distinct from the status of the enclosing message.)

## E-mail Users Want a Familiar Experience {#similar-ux}

A person communicating over the Internet today often has many options for reaching their desired correspondent, including web-based bulletin boards, contact forms, and instant messaging services.

E-mail offers a few distinctions from these other systems, most notably features like:

- Ubiquity: Most correspondents will have an e-mail address, while not everyone is present on the various non-e-mail messaging services, particularly those that have reliable end-to-end cryptographic protections,
- Federation: interaction between users on distinct domains who have not agreed on a common communications provider is still possible, and
- User Control: the user can interact with the e-mail system using an MUA of their choosing, including automation and other control over their preferred and/or customized workflow.

Other systems (like some popular instant messaging applications, such as WhatsApp and Signal Private Messenger) offer built-in end-to-end cryptographic protections by default, which are simpler for the user to understand.
("All the messages I see on Signal are confidential and integrity-protected" is a clean user story)

A user of e-mail is likely using e-mail instead of other systems because of the distinctions outlined above.
When adding end-to-end cryptographic protection to an e-mail endpoint, care should be taken not to negate any of the distinct features of e-mail as a whole.
If these features are violated to provide end-to-end crypto, the user may just as well choose one of the other systems that don't have the drawbacks that e-mail has.
Implementers should try to provide end-to-end protections that retain the familiar experience of e-mail itself.

Furthermore, an e-mail user is likely to regularly interact with other e-mail correspondents who *cannot* handle or produce end-to-end cryptographic protections.
Care should be taken that enabling cryptography in an MUA does not inadvertently limit the ability of the user to interact with correspondents who use legacy or non-cryptographic MUAs.

## Warning About Failure vs. Announcing Success {#security-indicators}

Moving the web from http to https offers useful historical similarities to adding end-to-end encryption to e-mail.

In particular, the indicators of what is "secure" vs. "insecure" for web browsers have changed over time.
For example, years ago the default experience was http, and https sites were flagged with "secure" indicators like a lock icon.
Starting in 2018, some browsers reversed that process by downplaying https, and instead visibly marking http as "not secure" (see [chrome-indicators]).

By analogy, when the user of an MUA first enables end-to-end cryptographic protection, it's likely that they will want to see which messages *have* protection (that is, the security indicators amenable to a conformant MUA as of 2024 are most likely to be comparable to those of a pre-2018 web browser).
But a user whose private e-mail communications with a given correspondent, or within a given domain are known to be entirely end-to-end protected might instead want to know which messages do *not* have the expected protections.

Note also that some messages may be expected to be confidential, but other messages are expected to be public -- the types of protection (see {{types-of-protection}}) that apply to each particular message will be different.
And the types of protection that are *expected* to be present in any context might differ (for example, by sender, by thread, or by date).

It is out of scope for this document to define expectations about protections for any given message, but an implementer who cares about usable experience should be deliberate and judicious about the expectations their interface assumes that the user has in a given context.
See also {{expect-e2e}} for future work.

# Types of Protection {#types-of-protection}

A given message might be:

- signed,
- encrypted,
- both signed and encrypted, or
- none of the above.

Given that many e-mail messages offer no cryptographic protections, the user needs to be able to detect which protections are present for any given message.

## Simplified Mental Model {#simple-mental-model}

To the extent that an e-mail message actually does have end-to-end cryptographic protections, those protections need to be visible and comprehensible to the end users: both the sending user and the receiving user.
If either user is unaware of the protections, then they do not effectively extend all the way to the "end".

However, most users do not have (or want to have) a sophisticated mental model of what kinds of protections can be associated with a given message.
Even the four states above approach the limits of complexity for an interface for normal users.

While {{no-encrypted-only}} recommends avoiding deliberate creation of encrypted-only messages, some messages may end up in the encrypted-only state due to signature failure or certificate revocation.

A simple model for the receiving user could be that a message is in one of three normal states, which align with the only reasonable choices for message composition:

- Unprotected
- Verified (has a valid signature from the apparent sender of the message)
- Confidential (meaning, encrypted, with a valid signature from the apparent sender of the message)

However, one error state exists for received mail that does not correspond to a reasonable choice for message composition:

- Encrypted But Unverified (meaning, encrypted without a valid signature from the apparent sender of the message)

Note that this last state is not "Confidential" (a secret shared exclusively between the participants in the communication) because the recipient does not know for sure who sent it.

In an ecosystem where encrypted-only messages are never deliberately sent (see {{no-encrypted-only}}), representing an Encrypted But Unverified message as a type of user-visible error is not unreasonable.
However, this is not the state of the global e-mail ecosystem when this document was written, since some legacy MUAs permit sending encrypted-but-unsigned mail (see {{expect-e2e}} for possible future guidance).

Alternately, an MUA may prefer to represent the state of a Encrypted but Unverified message to the user as though it was Unprotected, since no verification is possible.
However the MUA represents the message to the user, though, it MUST NOT leak cleartext of an encrypted message (even an Encrypted but Unverified message) in subsequent replies (see {{composing-reply}}) or similar replications of the message.

Note that a cleartext message with an invalid signature SHOULD NOT be represented to the user as anything other than Unprotected (see {{signature-failures}}), unless the MUA is providing the user with debugging information.

At the time this document was written, the global e-mail ecosystem contains a heterogeneous mix of legacy and non-cryptographic MUAs.
In such an ecosystem, a conformant MUA may prefer instead to represent "Verified" and "Encrypted" as orthogonal states of any given received message.
While this model does not precisely match the choice a user makes when composing a message, it may align more with the reality of the range of messages they receive.

## One Cryptographic Status Per Message

Some MUAs may attempt to generate multiple copies of a given e-mail message, with different copies offering different types of protection (for example, opportunistically encrypting on a per-recipient basis).
A message resulting from this approach will have a cryptographic state that few users will understand.
Even if the sender understands the different statuses of the different copies, the recipients of the messages may not understand (each recipient might not even know about the other copies).
See for example the discussion in {{mixed-recipients}} for how this can go wrong.

For comprehensibility, a conformant MUA SHOULD NOT create multiple copies of a given message that differ in the types of end-to-end cryptographic protections afforded.

For opportunistic cryptographic protections that are not surfaced to the user (that is, that are not end-to-end), other mechanisms like transport encryption ({{?RFC3207}}) or domain-based signing ({{?RFC6376}}) may be preferable due to ease of implementation and deployment.
These opportunistic transport protections are orthogonal to the end-to-end protections described in this document.

To the extent that opportunistic message protections are made visible to the user for a given copy of a message, a conformant MUA will distinguish that status from the message's end-to-end cryptographic status.
But the potential confusion caused by rendering this complex, hybrid state may not be worth the value of additional knowledge gained by the user.
The benefits of opportunistic protections accrue (or don't) even without visibility to the user (see {{?RFC7435}}).

The user needs a single clear, simple, and correct indication about the end-to-end cryptographic status of any given message.
See {{cryptographic-summary}} for more details.

# Cryptographic MIME Message Structure {#cryptographic-structure}

Implementations use the structure of an e-mail message to establish (when sending) and understand (when receiving) the cryptographic status of the message.
This section establishes some conventions about how to think about message structure.

## Cryptographic Layers {#cryptographic-layer}

"Cryptographic Layer" refers to a MIME substructure that supplies some cryptographic protections to an internal MIME subtree.
The internal subtree is known as the "protected part" though of course it may itself be a multipart object.

In the diagrams below, <u>↧</u> indicates "decrypts to", and <u>⇩</u> indicates "unwraps to".

### S/MIME Cryptographic Layers

For S/MIME [RFC8551], there are four forms of Cryptographic Layers: multipart/signed, PKCS#7 signed-data, PKCS7 enveloped-data, PKCS7 authEnveloped-data.

#### S/MIME Multipart Signed Cryptographic Layer {#smime-multipart-signed}

    └┬╴multipart/signed; protocol="application/pkcs7-signature"
     ├─╴[protected part]
     └─╴application/pkcs7-signature

This MIME layer offers authentication and integrity.

#### S/MIME PKCS7 signed-data Cryptographic Layer {#smime-pkcs7-signed-data}

    └─╴application/pkcs7-mime; smime-type="signed-data"
     ⇩ (unwraps to)
     └─╴[protected part]

This MIME layer offers authentication and integrity.

#### S/MIME PKCS7 enveloped-data Cryptographic Layer {#smime-pkcs7-enveloped-data}

    └─╴application/pkcs7-mime; smime-type="enveloped-data"
     ↧ (decrypts to)
     └─╴[protected part]

This MIME layer offers confidentiality.

#### S/MIME PKCS7 authEnveloped-data Cryptographic Layer {#smime-pkcs7-authenveloped-data}

    └─╴application/pkcs7-mime; smime-type="authEnveloped-data"
     ↧ (decrypts to)
     └─╴[protected part]

This MIME layer offers confidentiality and integrity.

Note that `enveloped-data` ({{smime-pkcs7-enveloped-data}}) and `authEnveloped-data` ({{smime-pkcs7-authenveloped-data}}) have identical message structure and very similar confidentiality semantics.
The only difference between the two is ciphertext malleability.

The examples in this document only include `enveloped-data`, but the implications for that layer apply to `authEnveloped-data` as well.

#### PKCS7 Compression is NOT a Cryptographic Layer

The Cryptographic Message Syntax (CMS) provides a MIME compression layer (`smime-type="compressed-data"`), as defined in {{?RFC3274}}.
While the compression layer is technically a part of CMS, it is not considered a Cryptographic Layer for the purposes of this document.

### PGP/MIME Cryptographic Layers

For PGP/MIME [RFC3156] there are two forms of Cryptographic Layers, signing and encryption.

#### PGP/MIME Signing Cryptographic Layer (multipart/signed) {#pgpmime-multipart-signed}

    └┬╴multipart/signed; protocol="application/pgp-signature"
     ├─╴[protected part]
     └─╴application/pgp-signature

This MIME layer offers authenticity and integrity.

#### PGP/MIME Encryption Cryptographic Layer (multipart/encrypted) {#pgpmime-multipart-encrypted}

    └┬╴multipart/encrypted
     ├─╴application/pgp-encrypted
     └─╴application/octet-stream
      ↧ (decrypts to)
      └─╴[protected part]

This MIME layer can offer any of:

- confidentiality (via a Symmetrically Encrypted Data Packet, see {{Section 5.7 of ?I-D.ietf-openpgp-crypto-refresh-13}}; an MUA MUST NOT generate this form due to ciphertext malleability)
- confidentiality and integrity (via a Symmetrically Encrypted Integrity Protected Data Packet (SEIPD), see {{Section 5.13 of I-D.ietf-openpgp-crypto-refresh-13}}), or
- confidentiality, integrity, and authenticity all together (by including an OpenPGP Signature Packet within the SEIPD).

## Cryptographic Envelope

The Cryptographic Envelope is the largest contiguous set of Cryptographic Layers of an e-mail message starting with the outermost MIME type (that is, with the Content-Type of the message itself).

If the Content-Type of the message itself is not a Cryptographic Layer, then the message has no cryptographic envelope.

"Contiguous" in the definition above indicates that if a Cryptographic Layer is the protected part of another Cryptographic Layer, the layers together comprise a single Cryptographic Envelope.

Note that if a non-Cryptographic Layer intervenes, all Cryptographic Layers within the non-Cryptographic Layer *are not* part of the Cryptographic Envelope.
They are Errant Cryptographic Layers (see {{errant-cryptographic-layers}}).

Note also that the ordering of the Cryptographic Layers implies different cryptographic properties.
A signed-then-encrypted message is different than an encrypted-then-signed message.
See {{sign-then-encrypt}}.

## Cryptographic Payload

The Cryptographic Payload of a message is the first non-Cryptographic Layer -- the "protected part" -- within the Cryptographic Envelope.

## Types of Cryptographic Envelope

### Simple Cryptographic Envelopes {#simple-cryptographic-envelopes}

As described above, if the "protected part" identified in the section above is not itself a Cryptographic Layer, that part *is* the Cryptographic Payload.

If the application wants to generate a message that is both encrypted and signed, it MAY use the simple MIME structure from {{pgpmime-multipart-encrypted}} by ensuring that the [I-D.ietf-openpgp-crypto-refresh-13] Encrypted Message within the `application/octet-stream` part contains an [I-D.ietf-openpgp-crypto-refresh-13] Signed Message (the final option described in {{pgpmime-multipart-encrypted}}).

### Multilayer Cryptographic Envelopes {#multilayer-cryptographic-envelopes}

It is possible to construct a Cryptographic Envelope consisting of multiple layers with either S/MIME or PGP/MIME , for example using the following structure:

    A └─╴application/pkcs7-mime; smime-type="enveloped-data"
    B  ↧ (decrypts to)
    C  └─╴application/pkcs7-mime; smime-type="signed-data"
    D   ⇩ (unwraps to)
    E   └─╴[protected part]

When handling such a message, the properties of the Cryptographic Envelope are derived from the series `A`, `C`.

As noted in {{simple-cryptographic-envelopes}}, PGP/MIME applications also have a simpler MIME construction available with the same cryptographic properties.

## Errant Cryptographic Layers {#errant-cryptographic-layers}

Due to confusion, malice, or well-intentioned tampering, a message may contain a Cryptographic Layer that is not part of the Cryptographic Envelope.
Such a layer is an Errant Cryptographic Layer.

An Errant Cryptographic Layer MUST NOT contribute to the message's overall cryptographic state.

Guidance for dealing with Errant Cryptographic Layers can be found in {{errant-layers}}.

### Mailing List Wrapping {#mailing-list-wrapping}

Some mailing list software will re-wrap a well-formed signed message before re-sending to add a footer, resulting in the following structure seen by recipients of the e-mail:

    H └┬╴multipart/mixed
    I  ├┬╴multipart/signed
    J  │├─╴text/plain
    K  │└─╴application/pgp-signature
    L  └─╴text/plain

In this message, `L` is the footer added by the mailing list.  `I` is now an Errant Cryptographic Layer.

Note that this message has no Cryptographic Envelope at all.

It is NOT RECOMMENDED to produce e-mail messages with this structure, because a legacy MUA may present the data in part `L` as though it were part of `J`, though they have different cryptographic properties.
In particular, if the user believes that the entire message is signed, but cannot distinguish `L` from `J` then the author of `L` can effectively tamper with content of the signed message, breaking the user's expectation of integrity and authenticity.

### A Baroque Example {#baroque-example}

Consider a message with the following overcomplicated structure:

    M └┬╴multipart/encrypted
    N  ├─╴application/pgp-encrypted
    O  └─╴application/octet-stream
    P   ↧ (decrypts to)
    Q   └┬╴multipart/signed
    R    ├┬╴multipart/mixed
    S    │├┬╴multipart/signed
    T    ││├─╴text/plain
    U    ││└─╴application/pgp-signature
    V    │└─╴text/plain
    W    └─╴application/pgp-signature

The 3 Cryptographic Layers in such a message are rooted in parts `M`, `Q`, and `S`.
But the Cryptographic Envelope of the message consists only of the properties derived from the series `M`, `Q`.
The Cryptographic Payload of the message is part `R`.
Part `S` is an Errant Cryptographic Layer.

Note that this message has both a Cryptographic Envelope *and* an Errant Cryptographic Layer.

It is NOT RECOMMENDED to generate messages with such complicated structures.
Even if a receiving MUA can parse this structure properly, it is nearly impossible to render in a way that the user can reason about the cryptographic properties of part `T` compared to part `V`.

## Cryptographic Summary {#cryptographic-summary}

The cryptographic status of an e-mail message with end-to-end cryptographic protections is known as the Cryptographic Summary.
A reasonable, simple Cryptographic Summary is derived from the aggregate properties of the layers in the Cryptographic Envelope.
This is a conceptual tool and a feature that an MUA can use to guide behavior and user experience, but it is not necessarily always directly exposed in any given user interface.
See {{well-formed}} for guidance and considerations about rendering the Cryptographic Summary to the user.

# Message Composition

This section describes the ideal composition of an e-mail message with end-to-end cryptographic protection.
A message composed with this form is most likely to achieve its end-to-end security goals.

## Message Composition Algorithm {#composition}

This section roughly describes the steps that an MUA should use to compose a cryptographically-protected message that has a proper cryptographic envelope and payload.

The message composition algorithm takes three parameters:

- `origbody`: the traditional unprotected message body as a well-formed MIME tree (possibly just a single MIME leaf part).
  As a well-formed MIME tree, `origbody` already has structural header fields present (see {{structural-header-fields}}).
- `origheaders`: the intended non-structural header fields for the message, represented here as a list of `(h,v)` pairs, where `h` is a header field name and `v` is the associated value.
- `crypto`: The series of cryptographic protections to apply (for example, "sign with the secret key corresponding to X.509 certificate X, then encrypt to X.509 certificates X and Y").
  This is a routine that accepts a MIME tree as input (the Cryptographic Payload), wraps the input in the appropriate Cryptographic Envelope, and returns the resultant MIME tree as output.

The algorithm returns a MIME object that is ready to be injected into the mail system:

- Apply `crypto` to `origbody`, yielding MIME tree `output`
- For each header name and value `(h,v)` in `origheaders`:
  - Add header `h` to `output` with value `v`
- Return `output`

## Encryption Outside, Signature Inside {#sign-then-encrypt}

An e-mail message that is both signed and encrypted is signed *inside* the encryption, and not the other way around.
For example, when crafting an encrypted and signed message using a simple Cryptographic Envelope of a single layer ({{simple-cryptographic-envelopes}}) with PGP/MIME, the OpenPGP Encrypted Message object should contain an OpenPGP Signed Message.
Likewise, when using a multilayer Cryptographic Envelope ({{multilayer-cryptographic-envelopes}}), the outer layer should be an encryption layer and the inner layer should be a signing layer.

Putting the signature inside the encryption has two advantages:

- The details of the signature remain confidential, visible only to the parties capable of decryption.
- Any mail transport agent that modifies the message is unlikely to be able to accidentally break the signature.

A conformant MUA MUST NOT generate an encrypted and signed message where the only signature is outside the encryption.

## Avoid Offering Encrypted-only Messages {#no-encrypted-only}

When generating an e-mail, the user has options about what forms of end-to-end cryptographic protections to apply to it.

In some cases, offering any end-to-end cryptographic protection is harmful: it may confuse the recipient and offer no benefit.

In other cases, signing a message is useful (authenticity and integrity are desirable) but encryption is either impossible (for example, if the sender does not know how to encrypt to all recipients) or meaningless (for example, an e-mail message to a mailing list that is intended to be published to a public archive).

In other cases, full end-to-end confidentiality, authenticity, and integrity are desirable.

There is no common use case for generating an e-mail message with end-to-end confidentiality but without authenticity or integrity.

A conformant MUA will keep its message composition interface simple, so when presenting the user with a choice of cryptographic protection, it MUST offer no more than three choices:

- no end-to-end cryptographic protection
- Verified (signed only)
- Confidential (signed and encrypted)

Note that these choices correspond to the simplified mental model in {{simple-mental-model}}.

The alternative approach, offering the user two boolean choices during message composition (one choice about signing, another choice about encryption) is a common anti-pattern among legacy MUAs, and creates additional usability hurdles for normal users:

- A user who wants to send a signed and encrypted message will have to click two buttons instead of one.
- A user who clicks "Encrypt" but neglects to click "Signed" may not understand that they are creating a message which cannot be authenticated by the receiver.

## Composing a Reply Message {#composing-reply}

When replying to a message, most MUAs compose an initial draft of the reply that contains quoted text from the original message.
A responsible MUA will take precautions to avoid leaking the cleartext of an encrypted message in such a reply.

If the original message was end-to-end encrypted, the replying MUA MUST either:

- compose the reply with end-to-end encryption, or
- avoid including quoted text from the original message.

In general, MUAs SHOULD prefer the first option: to compose an encrypted reply.
This is what users expect.

However, in some circumstances, the replying MUA cannot compose an encrypted reply.
For example, the MUA might not have a valid, unexpired, encryption-capable certificate for all recipients.
This can also happen during composition when a user adds a new recipient into the reply, or manually toggles the cryptographic protections to remove encryption.

In this circumstance, the composing MUA SHOULD strip the quoted text from the original message, unless the user indicates that they are deliberately including previously confidential content in a non-confidential message.

Note additional nuance about replies to malformed messages that contain encryption in {{reply-to-errant-encryption}}.

# Message Interpretation

Despite the best efforts of well-intentioned senders to create e-mail messages with well-formed end-to-end cryptographic protection, receiving MUAs will inevitably encounter some messages with malformed end-to-end cryptographic protection.

This section offers guidance on dealing with both well-formed and malformed messages containing Cryptographic Layers.

## Rendering Well-formed Messages {#well-formed}

A message is well-formed when it has a Cryptographic Envelope, a Cryptographic Payload, and no Errant Cryptographic Layers.
Rendering a well-formed message is straightforward.

The receiving MUA evaluates and assembles the cryptographic properties of the Cryptographic Envelope into a Cryptographic Summary and displays that status to the user in a secure, strictly-controlled part of the UI.
In particular, the part of the UI used to render the Cryptographic Summary of the message MUST NOT be spoofable, modifiable, or otherwise controllable by the received message itself.
By analogy, consider the "lock" icon in the address bar of the web browser: regardless of the content of the webpage, the lock icon will only be displayed when the transport to the web server is adequately secured.

Aside from this Cryptographic Summary, the message itself MUST be rendered as though the Cryptographic Payload is the body of the message.
The Cryptographic Layers themselves SHOULD NOT be rendered as distinct objects unless the MUA is providing the user with debugging information.

## Errant Cryptographic Layers {#errant-layers}

If an incoming message has any Errant Cryptographic Layers, a conformant interpreting MUA MUST ignore those layers when rendering the Cryptographic Summary of the message to the user.

### Errant Signing Layer

When rendering a message with an Errant Cryptographic Layer that provides authenticity and integrity (via signatures), the message should be rendered by replacing the Cryptographic layer with the part it encloses.

For example, a message with this structure:

    A └┬╴multipart/mixed
    B  ├╴text/plain
    C  ├┬╴multipart/signed
    D  │├─╴image/jpeg
    E  │└─╴application/pgp-signature
    F  └─╴text/plain

Should be rendered identically to this:

    A └┬╴multipart/mixed
    B  ├─╴text/plain
    D  ├─╴image/jpeg
    F  └─╴text/plain

In such a situation, a conformant MUA MUST NOT indicate in the Cryptographic Summary that the message is signed.
It MUST indicate that the message is Unprotected.

#### Exception: Mailing List Footers

The use case described in {{mailing-list-wrapping}} is common enough in some contexts that a conformant MUA MAY decide to handle it as a special exception.

If the MUA determines that the message comes from a mailing list (for example, it has a `List-ID` header), and it has a structure that appends a footer to a signing-only Cryptographic Layer with a valid signature, such as:

    H └┬╴multipart/mixed
    I  ├┬╴multipart/signed
    J  │├─╴[protected part, may be arbitrary MIME subtree]
    K  │└─╴application/{pgp,pkcs7}-signature
    L  └─╴[footer, typically text/plain]

or:

    H └┬╴multipart/mixed
    I  ├─╴application/pkcs7-mime; smime-type="signed-data"
       │⇩ (unwraps to)
    J  │└─╴[protected part, may be an arbitrary MIME subtree]
    L  └─╴[footer, typically text/plain]

Then, the MUA MAY indicate to the user that this is a Verified message that has been wrapped by the mailing list.

In this case, the MUA MUST distinguish the footer (part `L`) from the protected part (part `J`) when rendering any information about the signature.

One way to do this is to offer the user two different views of the message: the "mailing list" view, which hides any positive Cryptographic Summary but shows the footer:

    Cryptographic Protections: Unprotected
    H └┬╴multipart/mixed
    J  ├─╴[protected part, may be arbitrary MIME subtree]
    L  └─╴[footer, typically text/plain]

or the "sender's view", which shows the Cryptographic Summary as Verified, but hides the footer:

    Cryptographic Protections: Verified [details from part I]
    J └─╴[protected part, may be arbitrary MIME subtree]

### Errant Encryption Layer {#errant-encryption-layer}

An MUA may encounter a message with an Errant Cryptographic Layer that offers confidentiality (encryption), and the MUA is capable of decrypting it.

The user wants to be able to see the contents of any message that they receive, so a conformant MUA in this situation SHOULD decrypt the part.

In this case, though, a conformant MUA MUST NOT indicate in the message's Cryptographic Summary that the message itself was encrypted.
Such an indication could be taken to mean that other (non-encrypted) parts of the message arrived with cryptographic confidentiality.

Furthermore, when decrypting an Errant Cryptographic Layer, the MUA MUST treat the decrypted cleartext as a distinct MIME subtree, and not attempt to merge or splice it together with any other part of the message.
This offers protection against the direct exfiltration (also known as EFAIL-DE) attacks described in [EFAIL] and so-called `multipart/oracle` attacks described in [ORACLE].

#### Replying to a Message with an Errant Encryption Layer {#reply-to-errant-encryption}

Note that there is an asymmetry here between rendering and replying to a message with an Errant Encryption Layer.

When rendering, the MUA does not indicate that the message was encrypted, even if some subpart of it was decrypted for rendering.

When composing a reply to a message that has any encryption layer, even an errant one, the reply message SHOULD be marked for encryption, unless quoted and attributed text is not included in the reply, as noted in {{composing-reply}}.

When composing a reply to a message with an errant cryptographic layer, a conformant MUA MUST NOT decrypt any errant cryptographic layers when generating quoted or attributed text.
This will typically mean either leaving the ciphertext itself in the generated reply message, or simply not generating any quoted or attributed text at all.
This offers protection against the reply-based attacks described in [REPLY].

In all circumstances, if the reply message cannot be encrypted (or if the user elects to not encrypt the reply), the composed reply MUST NOT include any material from the decrypted subpart.

### Avoiding Non-MIME Cryptographic Mechanisms

In some cases, there may be a cryptographic signature or encryption that does not coincide with a MIME boundary.
For example so-called "PGP Inline" messages typically contain base64-encoded ("ASCII-armored", see {{Section 6 of I-D.ietf-openpgp-crypto-refresh-13}}) ciphertext, or within the content of a MIME part.

#### Do Not Validate Non-MIME Signatures

When encountering cryptographic signatures in these positions, a conformant MUA MUST NOT attempt to validate any signature.
It is challenging to communicate to the user exactly which part of such a message is covered by the signature, so it is better to leave the message marked as Unprotected.
See [SPOOFING] for examples of spoofed message signatures that rely on permissive legacy clients that are willing to validate signatures in poorly-structured messages.

#### Skip or Isolate Non-MIME Decryption When Rendering

When encountering what appears to be encrypted data not at a MIME boundary, a conformant MUA MAY decline to decrypt the data at all.

During message rendering, if a conformant MUA attempts decryption of such a non-MIME encrypted section of an e-mail, it MUST synthesize a separate MIME part to contain only the decrypted data, and not attempt to merge or splice that part together with any other part of the message.
Keeping such a section distinct and isolated from any other part of the message offers protection against the direct exfiltration attacks (also known as EFAIL-DE) described in [EFAIL].

#### Do Not Decrypt Non-MIME Decryption when Replying

When composing a reply to a message with such a non-MIME encrypted section, a conformant MUA MUST NOT decrypt any non-MIME encrypted section when generating quoted or attributed text, similar to the guidance in {{reply-to-errant-encryption}}.

This offers protection against the reply-based attacks described in [REPLY].

## Forwarded Messages with Cryptographic Protection

An incoming e-mail message may include an attached forwarded message, typically as a MIME subpart with `Content-Type: message/rfc822` ({{?RFC5322}}) or `Content-Type: message/global` ({{?RFC5355}}).

Regardless of the cryptographic protections and structure of the incoming message, the internal forwarded message may have its own Cryptographic Envelope.

The Cryptographic Layers that are part of the Cryptographic Envelope of the forwarded message are not Errant Cryptographic Layers of the surrounding message -- they are simply layers that apply to the forwarded message itself.

A conformant rendering MUA MUST NOT conflate the cryptographic protections of the forwarded message with the cryptographic protections of the incoming message.

A conformant rendering MUA MAY render a Cryptographic Summary of the protections afforded to the forwarded message by its own Cryptographic Envelope, as long as that rendering is unambiguously tied to the forwarded message itself, and cannot be spoofed either by the enclosing message or by the forwarded message.

## Signature failures {#signature-failures}

A cryptographic signature may fail in multiple ways.
A conformant receiving MUA that discovers a failed signature treats the message as though the signature did not exist.
This is similar to the standard guidance for about failed DKIM signatures (see {{Section 6.1 of RFC6376}}).

A conformant MUA MUST NOT render a message with a failed signature as more dangerous or more dubious than a comparable message without any signature at all.
In both cases, the Cryptographic Summary should be Unprotected.

A conformant MUA that encounters an encrypted-and-signed message where the signature is invalid SHOULD treat the message the same way that it would treat a message that is encryption-only, unless the MUA is providing the user with debugging information.

Some different ways that a signature may be invalid on a given message:

- the signature is not cryptographically valid (the math fails).
- the signature relies on suspect cryptographic primitives (e.g., over a deprecated digest algorithm, or was made by a weak key, e.g., 1024-bit RSA); note that this requires the rendering MUA to have an explicit policy about what cryptographic primitives are acceptable.
- the signature is made by a certificate which the receiving MUA does not have access to.
- the certificate used to verify the signature was revoked.
- the certificate used to verify the signature was expired at the time that the signature was made.
- the certificate used to verify the signature does not correspond to the author of the message. (for X.509, there is no subjectAltName of type rfc822Name whose value matches an e-mail address found in `From:` or `Sender:`)
- the certificate used to verify the signature was not issued by an authority that the MUA user is willing to rely on for certifying the sender's e-mail address, and the user has no other reasonable indication that the certificate belongs to the sender's e-mail address.
- the signature indicates that it was made at a time much before or much after from the date of the message itself.
- The signature covers a message that depends on an external subresource that might change (see {{external-subresources}}).

A valid signature must pass all these tests, but of course invalid signatures may be invalid in more than one of the ways listed above.

## Weak Encryption

Sometimes, a MUA might encounter a message with a well-formed Cryptographic Envelope that uses a form of encryption with substantial known flaws.
For example, a PGP/MIME message might use a Symmetrically Encrypted Data packet, which is known to have malleable ciphertext (see {{Section 5.7 of I-D.ietf-openpgp-crypto-refresh-13}}).
ِAs another example, an S/MIME message may use an `enveloped-data` MIME part with a `contentEncryptionAlgorithm` of `rc2-cbc` with `rc2ParameterVersion` of 160, meaning a 40-bit key (see {{Section 5.2 of ?RFC3370}}), which is widely considered breakable via brute force with moderate hardware investment in 2024.
As cryptanalysis and hardware capacities advance, an implementation may widen the scope of what encryption mechanisms are considered weak.

A receiving MUA MUST warn the user that such a message has a known weakness.
The receiving MUA MAY decline to decrypt such a message at all.
If it decides to decrypt a message with a weak encryption layer, it MUST NOT indicate in the message's Cryptographic Summary that the message was encrypted, as the confidentiality of the message is suspect.
This is similar to the approach taken in {{errant-encryption-layer}} for messages with an Errant Encryption Layer.

Like the Errant Encryption Layer situation, there is an asymmetry between rendering and replying to a message with weak encryption.
The guidance in {{reply-to-errant-encryption}} should be followed when replying to a message with weak encryption as well.

A receiving MUA MAY also treat historically archived messages with weak encryption differently from modern messages.
For example, if an encryption algorithm was known to be weak in 2005, then a message that appears to have been encrypted with that algorithm in 1995 might decrypted with a warning, as an archival service.
But a message that appears to have been encrypted with that same weak algorithm in 2015 might be quarantined as a likely attack.

There are several possible ways to distinguish a historical message from a modern one, including:

- The message timestamp (e.g., the `Date` header field)
- The time the message was first observed on the network (e.g., delivery of a new message via IMAP from a mailbox that had recently checked)
- The timestamp in any signature observed in the message
- The message structure (a message composed using protocol elements that were invented after the encryption algorithm was known weak is likely to be an attack than a legitimate archival message)

# Reasoning about Message Parts

When generating or rendering messages, it is useful to know what parts of the message are likely to be displayed, and how.
This section introduces some common terms that can be applied to parts within the Cryptographic Payload.

## Main Body Part {#main-body-part}

When an e-mail message is composed or rendered to the user there is typically one main view that presents a (mostly textual) part of the message.

While the message itself may be constructed of several distinct MIME parts in a tree, the part that is rendered to the user is the "Main Body Part".

When rendering a message, one of the primary jobs of the receiving MUA is identifying which part (or parts) is the Main Body Part.
Typically, this is found by traversing the MIME tree of the message looking for a leaf node that has a primary content type of `text` (e.g., `text/plain` or `text/html`) and is not `Content-Disposition: attachment`.

MIME tree traversal follows the first child of every `multipart` node, with the exception of `multipart/alternative`.
When traversing a `multipart/alternative` node, all children should be scanned, with preference given to the last child node with a MIME type that the MUA is capable of rendering directly.

An MUA MAY offer the user a mechanism to prefer a particular MIME type within `multipart/alternative` instead of the last renderable child.
For example, a user may explicitly prefer a `text/plain` alternative part over `text/html`.
Note that due to uncertainty about the capabilities and configuration of the receiving MUA, a conformant composing MUA should consider that multiple parts might be rendered as the Main Body Part when the message is ultimately viewed.
In particular, the composing MUA should ensure that any part likely to be viewed as the Main Body Part has the same semantic content as any other such part.

When composing a message, an originating MUA operating on behalf of an active user can identify which part (or parts) are the "main" parts: these are the parts the MUA generates from the user's editor.
Tooling that automatically generates e-mail messages should also have a reasonable estimate of which part (or parts) are the "main" parts, as they can be programmatically identified by the message author.

For a filtering program that attempts to transform an outbound message without any special knowledge about which parts are Main Body Parts, it can identify the likely parts by following the same routine as a receiving MUA.

## Attachments {#attachments}

A message may contain one or more separated MIME parts that are intended for download or extraction.
Such a part is commonly called an "attachment", and is commonly identified by having `Content-Disposition: attachment`.

An MUA MAY identify a subpart as an attachment, or permit extraction of a subpart even when the subpart does not have `Content-Disposition: attachment`.

When generating a message with end-to-end cryptographic protection, any attachment MUST be included within the Cryptographic Payload.
If an attachment is found outside the Cryptographic Payload, then the message is not well-formed (see {{well-formed}}), and will not be handled by other MUAs as intended.

Some MUAs have tried to compose messages where each attachment is placed in its own cryptographic envelope.
Such a message is problematic for several reasons:

- The attachments can be stripped, replaced, or reordered without breaking any cryptographic integrity mechanism.
- The resulting message may have a mix of cryptographic statuses (e.g., if a signature on one part fails but another succeeds, or if one part is encrypted and another is not).
  This mix of statuses is difficult to represent to the user in a comprehensible way.
- The divisions between the different attachments are visible to operators of any mail transport agent (MTA) that handles the message, potentially resulting in a metadata leak.
  For example, the MTA operator may learn the number of attachments, and the size of each attachment.

These messages are unlikely to be usefully interoperable without additional standardization work (see {{split-attachments}}).

## MIME Part Examples

Consider a common message with the following MIME structure:

~~~
M └─╴application/pkcs7-mime
   ↧ (decrypts to)
N  └─╴application/pkcs7-mime
    ⇩ (unwraps to)
O   └┬╴multipart/mixed
P    ├┬╴multipart/alternative
Q    │├─╴text/plain
R    │└─╴text/html
S    └─╴image/png
~~~

Parts `M` and `N` comprise the Cryptographic Envelope.

Parts `Q` and `R` are both Main Body Parts.

If part `S` is `Content-Disposition: attachment`, then it is an attachment.
If part `S` has no `Content-Disposition` header, it is potentially ambiguous whether it is an attachment or not.
If the sender prefers a specific behavior, it should explicitly set the `Content-Disposition` header on part `S` to either `inline` or `attachment` as guidance to the receiving MUA.

Consider also this alternate structure:

~~~
M └─╴application/pkcs7-mime
   ↧ (decrypts to)
N  └─╴application/pkcs7-mime
    ⇩ (unwraps to)
O   └┬╴multipart/alternative
P    ├─╴text/plain
Q    └┬╴multipart/related
R     ├─╴text/html
S     └─╴image/png
~~~

In this case, parts `M` and `N` still comprise the Cryptographic Envelope.

Parts `P` and `R` (the first two leaf nodes within each subtree of part `O`) are the Main Body Parts.

Part `S` is more likely not to be an attachment, as the subtree layout suggests that it is only relevant for the HTML version of the message.
For example, it might be rendered as an image within the HTML alternative.

# Certificate Management {#cert-management}

A cryptographically-capable MUA typically maintains knowledge about certificates for the user's own account(s), as well as certificates for the peers that it communicates with.

## Peer Certificates {#peer-certificates}

Most certificates that a cryptographically-capable MUA will use will be certificates belonging to the parties that the user communicates with through the MUA.
This section discusses how to manage the certificates that belong to such a peer.

The MUA will need to be able to discover X.509 certificates for each peer, cache them, and select among them when composing an encrypted message.

Detailed guidance about how to do this is beyond the scope of this document, but future revisions may bring it into scope (see {{more-peer-certs}}).

### Peer Certificate Selection {#peer-cert-selection}

When composing an encrypted message, the MUA needs to select an encryption-capable certificate for each recipient.

To select such a certificate for a given destination e-mail address, the MUA should look through all of its known certificates and verify that *all* of the conditions below are met:

- The certificate must be valid, not expired or revoked.
- It must have a subjectAltName of type rfc822Name whose contents match the destination e-mail address.
  In particular, the local-part of the two addresses should be an exact bytewise match, and the domain parts of the two addresses should be matched by ensuring label equivalence across the full domain name, as described in {{Section 2.3.2.4 of !RFC5890}}.
- The algorithm OID in the certificate's SPKI is known to the MUA and capable of encryption.
  Examples include:
  - rsaEncryption (OID 1.2.840.113549.1.1.1), with keyUsage (OID 2.5.29.15) extension present and the "key encipherment" bit (value 32) set.
  - curveX25519 (OID 1.3.101.110) with keyUsage extension present and the "key agreement" bit (value 8) set.
- If extendedKeyUsage (OID 2.5.29.37) is present, it contains at least one of the following OIDs: e-mail protection (OID 1.3.6.1.5.5.7.3.4), anyExtendedKeyUsage (OID 2.5.29.37.0).

A conformant MUA may include more considerations when selecting a peer certificate as well, see {{more-peer-cert-selection}} for examples.

## Local Certificates {#local-certificates}

The MUA also needs to know about one or more certificates associated with the user's e-mail account.
It is typically expected to have access to the secret key material associated with the public keys in those certificates.

While some basic guidance is offered here, it is beyond the scope of this document to describe all possible relevant guidance for local certificate and key material handling.
See {{more-local-certs}} for suggestions of guidance that a future version might bring into scope.

### Getting Certificates for the User {#local-certificate-setup}

If a conformant MUA does not have any certificate or secret key for the user, it should help the user to generate, acquire, or import them with a minimum of difficulty.

#### User Certificates for S/MIME {#local-cert-smime}

For S/MIME, the user SHOULD have both a signing-capable certificate and an encryption-capable certificate (and the corresponding secret keys).
Using the same cryptographic key material for multiple algorithms (i.e., for both encryption and signing) has been the source of vulnerabilities in other (non-e-mail) contexts (e.g., {{DROWN}} and {{IKE}}).
The simplest way to avoid any comparable risk is to use distinct key material for each cryptographic algorithm.
A conformant MUA that generates S/MIME certificates for the user MUST generate distinct S/MIME certificates: one for encryption and another for signing, to avoid possible cross-protocol key misuse.

The simplest option for an S/MIME-capable MUA is for the MUA to permit the user to import a PKCS #12 ({{?RFC7292}}) object that is expected to contain secret key material, end entity certificates for the user, and intermediate certification authority certificates that permit chaining from the end entity certs to widely-accepted trust anchors.
A conformant MUA that imports such a PKCS #12 bundle SHOULD warn the user if the bundle contains an S/MIME certificate and corresponding secret key where the same secret key is used for both encryption and signing.

An S/MIME-capable MUA that has access to user certificates and their corresponding secret key material should also offer the ability to export those objects into a well-formed PKCS #12 object that could be imported into another MUA operated by the same user.

Manual handling of PKCS #12 objects is challenging for most users.
Producing the initial PKCS #12 object typically can only be done with the aid of a certification authority via non-standardized, labor-intensive, and error-prone procedures that most users do not understand.
Furthermore, manual export and import incurs ongoing labor (for example, before certificate expiration) by the user which most users are unprepared to do (see {{local-cert-maintenance}}).

A better approach is for the MUA to integrate some form of automated certificate issuance procedure, for example, by using the ACME protocol for end user S/MIME certificates ({{?RFC8823}}).

Another possible approach is integration with a cryptographic hardware token or smartcard that can provide certificates and permit the use of isolated secret key material, for example [PKCS11], though this approach delegates the complexity of acquiring and managing certificates to management of the hardware token itself (see {{smartcards}}).

See also {{?I-D.woodhouse-cert-best-practice}} for more recommendations about managing user certificates.

#### User Certificates for PGP/MIME {#local-cert-pgp}

As distinct from S/MIME, OpenPGP ({{I-D.ietf-openpgp-crypto-refresh-13}}) has a different set of common practices.
For one thing, a single OpenPGP certificate can contain both a signing-capable key and a distinct encryption-capable key, so only one certificate is needed for an e-mail user of OpenPGP, as long as the certificate has distinct key material for the different purposes.

Furthermore, a single OpenPGP certificate MAY be only self-signed, so the MUA can generate such a certificate entirely on its own.

An OpenPGP-capable MUA should have the ability to import and export OpenPGP Tranferable Secret Keys (see {{Section 10.2 of I-D.ietf-openpgp-crypto-refresh-13}}), to enable manual transfer of user certificates and secret key material between multiple MUAs controlled by the user.

Since an OpenPGP certificate MAY be certified by third parties (whether formal certification authorities or merely other well-connected peers) the MUA SHOULD offer affordances to help the user acquire and merge third party certifications on their certificate.
When doing this, the MUA should prioritize third-party certifications from entities that the user's peers are likely to know about and be willing to rely on.

Since an OpenPGP certificate can grow arbitrarily large with third-party certifications, the MUA should assist the user in pruning it to ensure that it remains a reasonable size when transmitting it to other parties.

#### Generate Secret Key Material Locally {#local-key-generation}

Regardless of protocol used (S/MIME or PGP), when producing certificates for the end user, the MUA SHOULD ensure that it has generated secret key material locally, and MUST NOT accept secret key material from an untrusted external party as the basis for the user's certificate.
For example, a user who trusts their system administrator not to compromise their MUA may accept secret key material generated by the sysdmin, but probably should not accept secret key material generated by an unaffiliated online web service.

An MUA that accepts secret key material from a third party cannot prevent that third party from retaining this material.
A third party with this level of access could decrypt messages intended to be confidential for the user, or could forge messages that would appear to come from the user.

### Local Certificate Maintenance {#local-cert-maintenance}

In the context of a single e-mail account managed by an MUA, where that e-mail account is expected to be able to use end-to-end cryptographic protections, the MUA SHOULD warn the user (or proactively fix the problem) when/if:

- For S/MIME, the user's own certificate set for the account does not include a valid, unexpired encryption-capable X.509 certificate, and a valid, unexpired signature-capable X.509 certificate.
- For PGP/MIME, the user's own certificate does not include a valid, unexpired signing-capable key/subkey and a valid, unexpired encryption-capable key/subkey.
- Any of the user's own certificates for the account:
  - is due to expire in the next month (or at some other reasonable cadence).
  - not match the e-mail address associated with the account in question.
- Any of the user's own S/MIME certificates for the account:
  - does not have a keyUsage extension.
  - does not contain an extendedKeyUsage extension.
  - would be considered invalid by the MUA for any other reason if it were a peer certificate.

An MUA that takes active steps to fix any of these problems before they arise is even more usable than just warning, but guidance on how to do active certificate maintenance is beyond scope for this current document (see {{more-local-cert-maintenance}}).

If the MUA does find any of these issues and chooses to warn the user, it should use one aggregate warning with simple language that the certificates might not be acceptable for other people, and recommend a course of action that the user can take to remedy the problem.

### Shipping Certificates in Outbound Messages {#sending-certificates}

When sending mail, a conformant MUA SHOULD include copies of the user's own certificates (and potentially other certificates) in each message to facilitate future communication, unless it has specific knowledge that the other parties involved already know the relevant keys (for example, if it is mail between members within a domain that has a synchronized and up-to-date certificate directory).

The mechanism for including these certificates, and which certificates to include in the message, are protocol specific.

#### Shipping Certificates in S/MIME Messages

In any S/MIME SignedData object, certificates can be shipped in the "certificates" member.
In S/MIME EnvelopedData object, certificates can be shipped in the "originatorInfo.certs" member.

When a single S/MIME-protected e-mail message is encrypted and signed, it is usually sufficient to ship all the relevant certificates in the inner SignedData object's "certificates" member.

The S/MIME certificates shipped in such a message SHOULD include:

- The user's own S/MIME signing certificate, so that signature on the current message can be validated.
- The user's own S/MIME encryption-capable certificate, so that the recipient can reply in encrypted form.
- On an encrypted message to multiple recipients, the encryption-capable peer certificates of the other recipients, so that any recipient can easily "reply all" without needing to search for certificates.
- Any intermediate CA certificates needed to chain all of the above to a widely-trusted set of root authorities.

#### Shipping Certificates in PGP/MIME Messages

PGP/MIME does not have a single specific standard location for shipping certificates.

Some MUAs ship relevant OpenPGP certificates in a single MIME leaf of Content-Type "application/pgp-keys".
When such a message has cryptographic protections, to ensure that the message is well-formed, this kind of MIME part SHOULD be a leaf of the Cryptographic Payload, and not outside of it.
One problem with this approach is that it appears to recipients with non-cryptographic MUAs as an "attachment", which can lead to confusion if the user does not know how to use it.

Other implementations ship relevant OpenPGP certificates in "Autocrypt" or "Autocrypt-Gossip" message header fields (see {{AUTOCRYPT}}).
To ensure that those header fields receive the same cryptographic authenticity as the rest of the message, these header fields SHOULD be protected as described in {{?I-D.ietf-lamps-header-protection}}.

The OpenPGP certificates shipped in such a message SHOULD include:

- The user's own OpenPGP certificate, capable of both signing and encryption, so that the user can validate the message's signature and can encrypt future messages
- On an encrypted message to multiple recipients, the OpenPGP certificates of the other recipients, so that any recipient can easily "reply all" without needing to search for certificates.

# Common Pitfalls and Guidelines {#common-pitfalls}

This section highlights a few "pitfalls" and guidelines based on these discussions and lessons learned.

## Reading Sent Messages {#reading-sent-messages}

When sending a message, a typical MUA will store a copy of the message sent in sender's Sent mail folder so that the sender can read it later.
If the message is an encrypted message, storing it encrypted requires some forethought to ensure that the sender can read it in the future.

It is a common and simple practice to encrypt the message not only to the recipients of the message, but also to the sender.
One advantage of doing this is that the message that is sent on the wire can be identical to the message stored in the sender's Sent mail folder.
This allows the sender to review and re-read the message even though it was encrypted.

There are at least three other approaches that are possible to ensure future readability by the sender of the message, but with different tradeoffs:

- Encrypt two versions of the message: one to the recipients (this version is sent on the wire), and one to the sender only (this version is stored in the sender's Sent folder).
  This approach means that the message stored in the Sent folder is not byte-for-byte identical to the message sent to the recipients.
  In the event that message delivery has a transient failure, the MUA cannot simply re-submit the stored message into the SMTP system and expect it to be readable by the recipient.

- Store a cleartext version of the message in the Sent folder.
  This presents a risk of information leakage: anyone with access to the Sent folder can read the contents of the message.
  Furthermore, any attempt to re-send the message needs to also re-apply the cryptographic transformation before sending, or else the message contents will leak upon re-send.
  A conformant MUA SHOULD NOT store a cleartext copy in the Sent folder unless it knows that the Sent folder cannot be read by an attacker.
  For example, if end-to-end confidentiality is desired, then storing the cleartext in an IMAP folder where a potentially adversarial server can read it defeats the purpose.

- A final option is that the MUA can store a copy of the message's encryption session key.
  Standard e-mail encryption mechanisms (e.g., S/MIME and PGP/MIME) are hybrid mechanisms: the asymmetric encryption steps simply encrypt a symmetric "session key", which is used to encrypt the message itself.
  If the MUA stores the session key itself, it can use the session key to decrypt the Sent message without needing the Sent message to be decryptable by the user's own asymmetric key.
  An MUA doing this must take care to store (and backup) its stash of session keys, because if it loses them it will not be able to read the sent messages; and if someone else gains access to them, they can decrypt the sent messages.
  This has the additional consequence that any other MUA accessing the same Sent folder cannot decrypt the message unless it also has access to the stashed session key.

## Reading Encrypted Messages after Certificate Expiration

When encrypting a message, the sending MUA should decline to encrypt to an expired certificate (see {{peer-cert-selection}}).
But when decrypting a message, as long as the viewing MUA has access to the appropriate secret key material, it should permit decryption of the message, even if the associated certificate is expired.
That is, the viewing MUA should not prevent the user from reading a message that they have already received.

The viewing MUA may warn the user when decrypting a message that appears to have been encrypted to an encryption-capable certificate that was expired at the time of encryption (e.g., based on the `Date:` header field of the message, or the timestamp in the cryptographic signature), but otherwise should not complain.

The primary goal of certificate expiration is to facilitate rotation of secret key material, so that secret key material does not need to be retained indefinitely.
Certificate expiration permits the user to destroy an older secret key if access to the messages received under it is no longer necessary (see also {{secure-deletion}}).

## Unprotected Message Header Fields {#message-header-fields}

Many legacy cryptographically-aware MUAs only apply cryptographic protections to the body of the message, but leave the header fields unprotected.
This gives rise to vulnerabilities like information leakage (e.g., the Subject line is visible to a passive intermediary) or message tampering (e.g., the Subject line is replaced, effectively changing the semantics of a signed message).
These are not only security vulnerabilities, but usability problems, because the distinction between what is a header and what is the body of a message is unclear to many end users, and requires a more complex mental model than is necessary.
Useful security comes from alignment between simple mental models and tooling.

To avoid these concerns, a conformant MUA MUST implement header protection as described in {{!I-D.ietf-lamps-header-protection}}.

Note that some message header fields, like `List-*`, `Archived-At`, and `Resent-*` are typically added by an intervening MUA (see {{intervening-mua}}), not by one of the traditional "ends" of an end-to-end e-mail exchange.
A receiving MUA may choose to consider the contents of these header fields on an end-to-end protected message as markers added during message transit, even if they are not covered by the end-to-end cryptographic protection.

## Composing an Encrypted Message with Bcc {#bcc-variants}

When composing an encrypted message containing at least one recipient address in the `Bcc` header field, there is a risk that the encrypted message itself could leak information about the actual recipients, even if the `Bcc` header field does not mention the recipient.
For example, if the message clearly indicates which certificates it is encrypted to, the set of certificates can identify the recipients even if they are not named in the message header fields.

Because of these complexities, there are several interacting factors that need to be taken into account when composing an encrypted message with Bcc'ed recipients.

- {{Section 3.6.3 of RFC5322}} describes a set of choices about whether (and how) to populate the `Bcc` field explicitly on Bcc'ed copies of the message, and in the copy stored in the sender's `Sent` folder.
- When separate copies are made for `Bcc`ed recipients, should each separate copy *also* be encrypted to the named recipients, or just to the designated `Bcc` recipient?
- When a copy is stored in the `Sent` folder, should that copy also be encrypted to `Bcc`ed recipients? (see also {{reading-sent-messages}})
- When a message is encrypted, if there is a mechanism to include the certificates of the recipients, whose certificates should be included?

### Simple Encryption with Bcc {#bcc-recommendation}

Here is a simple approach that tries to minimize the total number of variants of the message created while leaving a coherent view of the message itself:

- No cryptographic payload contains any `Bcc` header field.
- The main copy of the message is signed and encrypted to all named recipients and to the sender.
  A copy of this message is also stored in the sender's `Sent` folder.
- Each `Bcc` recipient receives a distinct copy of the message, with an identical cryptographic payload, and the message is signed and encrypted to that specific recipient and all the named recipients.
  These copies are not stored in the sender's `Sent` folder.
- Any Bcc'ed recipient MUST NOT be taken into consideration when determining which certificates to include in the message.
  In particular, certificates for Bcc'ed recipients MUST NOT included in any message.

#### Rationale

The approach described in {{bcc-recommendation}} aligns the list of cryptographic recipients as closely as possible with the set of named recipients, while still allowing a `Bcc`ed recipient to read their own copy, and to "Reply All" should they want to.

This should reduce user confusion on the receiving side: a recipient of such a message who naively looks at the user-facing header fields from their own mailbox will have a good sense of what cryptographic treatments have been applied to the message.
It also simplifies message composition and user experience: the message composer sees fields that match their expectations about what will happen to the message.
Additionally, it may preserve the ability for a Bcc'ed recipient to retain their anonymity, should they need to offer the signed cryptographic payload to an outside party as proof of the original sender's intent without revealing their own identity.

## Draft Messages {#draft-messages}

When composing a message, most MUAs will save a copy of the as-yet-unsent message to a "Drafts" folder.
If that folder is itself stored somewhere not under the user's control (e.g., an IMAP mailbox), it would be a mistake to store the draft message in the clear, because its contents could leak.

This is the case even if the message is ultimately sent deliberately in the clear.
During message composition, the MUA does not know whether the message is intended to be sent encrypted or not.
For example, just before sending, the sender could decide to encrypt the message, and the MUA would have had no way of knowing.

The MUA SHOULD encrypt all draft messages, unless it has explicit knowledge that the message will not be encrypted when sent, or that the Drafts folder cannot be read by an attacker.
For example, if end-to-end confidentiality is desired, then storing a cleartext draft in an IMAP folder where a potentially adversarial server can read it defeats the purpose.

Furthermore, when encrypting a draft message, the message draft MUST only be encrypted to the user's own certificate, or to some equivalent secret key that only the user possesses.
A draft message encrypted in this way can be decrypted when the user wants to resume composing the message, but cannot be read by anyone else, including a potential intended recipient.
Note that a draft message encrypted in this way will only be resumable by another MUA attached to the same mailbox if that other MUA has access to the user's decryption-capable secret key.
This shared access to key material is also likely necessary for useful interoperability, but is beyond the scope of this document (see {{cross-mua-local-keys}}).

A conformant MUA MUST NOT sign a message draft with the user's normal signing key.
If draft signing is intended for cryptographic coordination between multiple MUAs of the same user, it should be negotiated with a different key (but see {{cross-mua-local-keys}}).

The message should only be encrypted to its recipients upon actually sending the message.
No reasonable user expects their message's intended recipients to be able to read a message that is not yet complete.

## Composing a Message to Heterogeneous Recipients {#mixed-recipients}

When sending a message that the user intends to be encrypted, it's possible that some recipients will be unable to receive an encrypted copy.
For example, when Carol composes a message to Alice and Bob, Carol's MUA may be able to find a valid encryption-capable certificate for Alice, but none for Bob.

In this situation, there are four possible strategies, each of which has a negative impact on the experience of using encrypted mail.
Carol's MUA can:

1. send encrypted to Alice and Bob, knowing that Bob will be unable to read the message.
2. send encrypted to Alice only, dropping Bob from the message recipient list.
3. send the message in the clear to both Alice and Bob.
4. send an encrypted copy of the message to Alice, and a cleartext copy to Bob.

Each of these strategies has different drawbacks.

The problem with approach 1 is that Bob will receive unreadable mail.

The problem with approach 2 is that Carol's MUA will not send the message to Bob, despite Carol asking it to.

The problem with approach 3 is that Carol's MUA will not encrypt the message, despite Carol asking it to.

Approach 4 has two problems:

- Carol's MUA will release a cleartext copy of the message, despite Carol asking it to send the message encrypted.

- If Alice wants to "reply all" to the message, she may not be able to find an encryption-capable certificate for Bob either.
  This puts Alice in an awkward and confusing position, one that users are unlikely to understand.
  In particular, if Alice's MUA is following the guidance about replies to encrypted messages in {{composing-reply}}, having received an encrypted copy will make Alice's Reply buffer behave in an unusual fashion.

This is particularly problematic when the second recipient is not "Bob" but in fact a public mailing list or other visible archive, where messages are simply never encrypted.

Carol is unlikely to understand the subtleties and negative downstream interactions involved with approaches 1 and 4, so presenting the user with those choices is not advised.

The most understandable approach for an MUA with an active user is to ask the user (when they hit "send") to choose between approach 2 and approach 3.
If the user declines to choose between 2 and 3, the MUA can drop them back to their message composition window and let them make alternate adjustments.

See also further discussion of these scenarios in {{?I-D.dkg-mail-cleartext-copy}}.

## Message Transport Protocol Proxy: A Dangerous Implementation Choice {#proxy-dangers}

An implementer of end-to-end cryptographic protections may be tempted by a simple software design that piggybacks off of a mail protocol like SMTP Submission ({{?RFC6409}}), IMAP ({{?RFC9051}}), or JMAP ({{?RFC8621}}) to handle message assembly and interpretation.
In such an architecture, a naive MUA speaks something like a "standard" protocol like SMTP, IMAP, or JMAP to a local proxy, and the proxy handles signing and encryption (outbound), and decryption and verification (inbound) internally on behalf of the user.
While such a "pluggable" architecture has the advantage that it is likely to be easy to apply to any mail user agent, it is problematic for the goals of end-to-end communication, especially in an existing cleartext ecosystem like e-mail, where any given message might be unsigned or signed, cleartext or encrypted.
In particular:

- the user cannot easily and safely identify what protections any particular message has (including messages currently being composed), and
- the proxy itself is unaware of subtle nuances about the message that the MUA actually knows.

With a trustworthy and well-synchronized sidechannel or protocol extension between the MUA and the proxy, it is possible to deploy such an implementation safely, but the requirement for the sidechannel or extension eliminates the universal-deployability advantage of the scheme.

Similar concerns apply to any implementation bound by an API which operates on message objects alone, without any additional contextual parameters.

This section attempts to document some of the inherent risks involved with such an architecture.

### Dangers of a Submission Proxy for Message Composition {#proxy-for-message-composition}

When composing and sending a message, the act of applying cryptographic protections has subtleties that cannot be directly expressed in the SMTP protocol used by Submission {{?RFC6409}}, or in any other simple protocol that hands off a cleartext message for further processing.

For example, the sender cannot indicate via SMTP whether or not a given message *should* be encrypted (some messages, like those sent to a publicly archived mailing list, are pointless to encrypt), or select among multiple certificates for a recipient, if they exist (see {{peer-cert-selection}}).

Likewise, because such a proxy only interacts with the message when it is ready to be sent, it cannot indicate back to the user *during message composition* whether or not the message is able to be encrypted (that is, whether a valid certificate is available for each intended recipient).
A message author may write an entirely different message if they know that it will be protected end-to-end; but without this knowledge, the author is obliged either to write text that they presume will be intercepted, or to risk revealing sensitive content.

Even without encryption, deciding whether to sign or not (and which certificate to sign with, if more than one exists) is another choice that the proxy is ill-equipped to make.
The common message-signing techniques either render a message unreadable by any non-cryptographic MUA (i.e., PKCS7 signed-data), or appear as an attachment that can cause confusion to a naive recipient using a non-cryptographic MUA (i.e., multipart/signed).
If the sender knows that the recipient will not check signatures, they may prefer to leave a cleartext message without a cryptographic signature at all.

Furthermore, handling encryption properly depends on the context of any given message, which cannot be expressed by the MUA to the Submission proxy.
For example, decisions about how to handle encryption and quoted or attributed text may depend on the cryptographic status of the message that is being replied to (see {{composing-reply}}).

Additionally, such a proxy would need to be capable of managing the user's own key and certificate (see {{local-certificates}}).
How will the implementation indicate to the user when their own certificate is near expiry, for example?
How will any other error conditions be handled when communication with the user is needed?

While an extension to SMTP might be able to express all the necessary semantics that would allow a generic MUA to compose messages with standard cryptographic protections via a proxy, such an extension is beyond the scope of this document.
See {{?I-D.ietf-jmap-smime-sender-extensions}} for an example of how an MUA using a proxy protocol might indicate signing and encryption instructions to its proxy.

### Dangers of an IMAP Proxy for Message Rendering

When receiving and rendering a message, the process of indicating to the user the cryptographic status of a message requires subtleties that are difficult to offer from a straightforwad IMAP (or POP {{?RFC1939}}, or JMAP) proxy.

One approach such a proxy could take is to remove all the Cryptographic Layers from a well-formed message, and to package a description of those layers into a special header field that the MUA can read.
But this merely raises the question: what semantics need to be represented?
For example:

- Was the message signed?  If so, by whom?  When?
- Should the details of the cryptographic algorithms used in any signatures found be indicated as well?
- Was the message encrypted?  if so, to whom?  What key was used to decrypt it?
- If both signed and encrypted, was the signing outside the encryption or inside?
- How should errant Cryptographic Layers (see {{errant-cryptographic-layers}}) be dealt with?
- What cryptographic protections do the header fields of the message have? (see {{?I-D.ietf-lamps-header-protection}})
- How are any errors or surprises communicated to the user?

If the proxy passes any of this cryptographic status to the client in an added header field, it must also ensure that no such header field is present on the messages it receives before processing it.
If it were to allow such a header field through unmodified to any client that is willing to trust its contents, an attacker could spoof the field to make the user believe lies about the cryptographic status of the message.
In order for an MUA to be confident in such a header field, then, it needs a guarantee from the proxy that any header it produces will be safe.
How does the MUA reliably negotiate this guarantee with the proxy?
If the proxy can no longer offer this guarantee, how will the MUA know that things have changed?

If such an inbound proxy handles certificate discovery in inbound messages (see {{peer-discovery-incoming}}), it will also need to communicate the results of that discovery process to its corresponding outbound proxy for message composition (see {{proxy-for-message-composition}}).

While an extension to IMAP (or POP, or JMAP) might be able to express all the necessary semantics that would allow a generic MUA to indicate standardized cryptographic message status, such an extension is beyond the scope of this document.
{{?RFC9219}} describes S/MIME signature verification status over JMAP, which is a subset of the cryptographic status information described here.

### Who Controls the Proxy?

Finally, consider that the naive proxy deployment approach is risky precisely because of its opacity to the end user.
Such a deployment could be placed anywhere in the stack, including on a machine that is not ultimately controlled by the end user, making it effectively a form of transport protection, rather than end-to-end protection.

An MUA explicitly under the control of the end user with thoughtful integration can offer UI/UX and security guarantees that a simple proxy cannot provide.
See also {{proxy-extensions}} for suggestions of future work that might augment a proxy to make it safer.

## Intervening MUAs Do Not Handle End-to-End Cryptographic Protections {#intervening-mua}

Some Mail User Agents (MUAs) will resend a message in identical form (or very similar form) to the way that they received it.
For example, consider the following use cases:

- A mail expander or mailing list that receives a message and re-sends it to all subscribers (see also {{mailing-lists}} for more discussion of mailing lists).
- An individual user who reintroduces a message they received into the mail transport system (see {{Section 3.6.6 of RFC5322}}).
- An automated e-mail intake system that forwards a report to the mailboxes of responsible staffers.

These MUAs intervene in message transport by receiving and then re-injecting messages into the mail transport system.
In some cases, the original sender or final recipient of a message that has passed through such an MUA may be unaware of the intervention.
(Note that an MUA that forwards a received message as a attachment (MIME subpart) of type `message/rfc822` or `message/global` or "inline" in the body of a message is *not* acting as an intervening MUA in this sense, because the forwarded message is encapsulated within a visible outer message that is clearly from the MUA itself.)

An intervening MUA should be aware of end-to-end cryptographic protections that might already exist on messages that they re-send.
In particular, it is unclear what the "end-to-end" properties are of a message that has been handled by an intervening MUA.
For signed-only messages, if the intervening MUA makes any substantive modifications to the message as it passes it along, it may break the signature from the original sender.
In many cases, breaking the original signature is the appropriate result, since the original message has been modified, and the original sender has no control over the modifications made by the intervening MUA.
For encrypted-and-signed messages, if the intervening MUA is capable of decrypting the message, it must be careful when re-transmitting the message.
Will the new recipient be able to decrypt it?
If not, will the message be useful to the recipient?
If not, it may not make sense to re-send the message.

Beyond the act of re-sending, an intervening MUA should not itself try to apply end-to-end cryptographic protections on a message that it is resending unless directed otherwise by some future specification.
Additional layers of cryptographic protection added in an ad-hoc way by an intervening MUA are more likely to confuse the recipient and will not be interpretable as end-to-end protections as they do not originate with the original sender of the message.

## External Subresources in MIME Parts Break Cryptographic Protections {#external-subresources}

A MIME part with a content type that can refer to external resources (like `text/html`) may itself have some sort of end-to-end cryptographic protections.
However, retrieving or rendering these external resources may violate the properties that users expect from cryptographic protection.

As a baseline, retrieving the external resource at the time a message is read can be used as a "web bug", leaking the activity and network location of the receiving user to the server hosting the external resource.
This privacy risk is present, of course, even for messages with no cryptographic protections, but may be even more surprising to users who are shown some level of security indicator about a given message.

Other problems with external resources are more specifically bound to cryptographic protections.

For example, an signed e-mail message with at `text/html` part that refers to an external image (i.e., via `<img src="https://example.com/img.png">`) may render differently if the hosting webserver decides to serve different content at the source URL for the image.
This effectively breaks the goals of integrity and authenticity that the user should be able to rely on for signed messages, unless the external subresource has strict integrity guarantees (e.g., via {{SRI}}).

Likewise, fetching an external subresource for an encrypted-and-signed message effectively breaks goals of privacy and confidentiality for the user.

This is loosely analogous to security indicator problems that arose for web browsers as described in {{mixed-content}}.
However, while fetching the external subresource over https is sufficient to avoid a "mixed content" warning from most browsers, it is insufficicent for an MUA that wants to offer its users true end-to-end guarantees for e-mail messages.

A conformant sending MUA that applies signing-only cryptographic protection to a new e-mail message with an external subresource should take one of the following options:

- pre-fetch the external subresource and include it in the message itself,
- use a strong integrity mechanism like Subresource Integrity ({{SRI}}) to guarantee the content of the subresource (though this does not fix the "web bug" privacy risk described above), or
- prompt the composing user to remove the subresource from the message.

A conformant sending MUA that applies encryption to a new e-mail message with an external resource cannot depend on subresource integrity to protect the privacy and confidentiality of the message, so it should either pre-fetch the external resource to include it in the message, or prompt the composing user to remove it before sending.

A conformant receiving MUA that encounters a message with end-to-end cryptographic protections that contain a subresource MUST either refuse to retrieve and render the external subresource, or it should decline to treat the message as having cryptographic protections.
For example, it could indicate in the Cryptographic Summary that the message is Unprotected.

Note that when composing a message reply with quoted text from the original message, if the original message did contain an external resource, the composing MUA SHOULD NOT fetch the external resource solely to include it in the reply message, as doing so would trigger the "web bug" at reply composition time.
Instead, the safest way to deal with quoted text that contains an external resource in an end-to-end encrypted reply is to strip any reference to the external resource during initial composition of the reply.

# IANA Considerations

This document does not require anything from IANA.

# Security Considerations

This entire document addresses security considerations about end-to-end cryptographic protections for e-mail messages.

# Acknowledgements

The set of constructs and recommendations in this document are derived from discussions with many different implementers, including
Bjarni Rúnar Einarsson,
Daniel Huigens,
David Bremner,
Deb Cooley,
Eliot Lear,
Fabian Ising,
Heiko Schaefer,
Holger Krekel,
Jameson Rollins,
John Levine,
Jonathan Hammell,
juga,
Patrick Brunschwig,
Paul Kyzivat,
Pete Resnick,
Roman Danyliw,
Santosh Chokhani,
Stephen Farrell, and
Vincent Breitmoser.

--- back

# Future Work

This document contains useful guidance for MUA implementers, but it cannot contain all possible guidance.
Future revisions to this document may want to further explore the following topics, which are out of scope for this version.

## Webmail Threat Model

The webmail threat model for end-to-end cryptographic protections is significantly more complex than the traditional MUA model.
For example, the web server hosting the webmail interface could be a potential adversary.
If the user treats the web server as a trusted party, but the web server violates that trust, the end-to-end cryptographic protections do not hold.

A future version of this document could include more detailed discussion of an adversarial threat model for end-to-end cryptographic protections in a webmail context.

## Test Vectors

A future version of this document (or a companion document) could contain examples of well-formed and malformed messages using cryptographic key material and certificates from {{I-D.ietf-openpgp-crypto-refresh-13}} and {{?RFC9216}}.

It may also include example renderings of these messages.

## Further Guidance on Peer Certificates {#more-peer-certs}

### Certificate Discovery from Incoming Messages {#peer-discovery-incoming}

As described in {{sending-certificates}}, an incoming e-mail message may have one or more certificates embedded in it.
This document currently acknowledges that receiving MUA should assemble a cache of certificates for future use, but providing more detailed guidance for how to assemble and manage that cache is currently out of scope.

Existing recommendations like {{AUTOCRYPT}} provide some guidance for handling incoming certificates about peers, but only in certain contexts.
A future version of this document may describe in more detail how these incoming certs should be handled.

### Certificate Directories {#peer-discovery--directory}

Some MUAs may have the capability to look up peer certificates in a directory, for example via LDAP {{?RFC4511}}, WKD {{?I-D.koch-openpgp-webkey-service}}, or the DNS (e.g., SMIMEA {{?RFC8162}} or OPENPGPKEY {{?RFC7929}} resource records).

A future version of this document may describe in more detail what sources an MUA should consider when searching for a peer's certificates, and what to do with the certificates found by various methods.

### Checking for Certificate Revocation {#cert-revocation}

A future version of this document could discuss how/when to check for revocation of peer certificates, or of the user's own certificate.

Such discussion should address privacy concerns: what information leaks to whom when checking peer cert revocations?

### Further Peer Certificate Selection {#more-peer-cert-selection}

A future version of this document may describe more prescriptions for deciding whether a peer certificate is acceptable for encrypting a message.
For example, if the SPKI is an EC Public Key and the keyUsage extension is absent, what should the encrypting MUA do?

A future version of this document might also provide guidance on what to do if multiple certificates are all acceptable for encrypting to a given recipient.
For example, the sender could select among them in some deterministic way; it could encrypt to all of them; or it could present them to the user to let the user select any or all of them.

### Human-readable Names in Peer Certificates, Header Fields, and Addressbooks {#display-names}

In header fields like `From:` that may contain a `display-name` as described in {{Section 3.4 of RFC5322}}, a malicious sender (or interfering adversary) may populate the `display-name` part with a human-readable name that does not at all match the actual name of the participant.
{{peer-cert-selection}} describes some matching rules relating peer certificates to e-mail addresses (the `addr-spec` part of these e-mail header fields), but does not contemplate matching `display-name`s or other or similar user-visible data elements.
{{signature-failures}} describes how signature validation should confirm a binding between the `addr-spec` and the certificate itself, but also does not contemplate matching `display-name`s or other similar user-visible data elements.
Depending on how the receiving MUA renders the `display-name` in a message's header field, that unvalidated field may present a risk of user confusion which could break the intended end-to-end assurances.
Yet both X.509 and OpenPGP certificate formats offer ways to provide cryptographically certified (though possibly not unique) comparable human-readable names.
Additionally, many MUAs also include an addressbook or comparable feature which can make substantive connections between user-relevant identity labels and e-mail addresses.

A human-readable name like a `display-name` does not have the property of global uniqueness that an `addr-spec` does, so reasoning about human-readable names and rendering them to the user as an element in a system providing end-to-end cryptographic assurance requires additional deliberate analysis.

A future version of this document might offer strategies for associating human-readable names from certificates (and features like addressbooks) to the rendering of header fields that include `display-name`.
Such guidance should be paired with an analysis of specific usability and security risks associated with these human-readable fields, as well as a description of how the recommended guidance mitigates those risks.

## Further Guidance on Local Certificates and Secret Keys {#more-local-certs}

### Cross-MUA sharing of Local Certificates and Secret Keys {#cross-mua-local-keys}

Many users today use more than one MUA to access the same mailbox (for example, one MUA on a mobile device, and another MUA on a desktop computer).

A future version of this document might offer guidance on how multiple MUAs attached to the same mailbox can efficiently and securely share the user's own secret key material and certificates between each other.
This guidance should include suggestions on how to maintain the user's keys (e.g., avoiding certificate expiration) and safe secret key transmission.

### Use of Smartcards or Other Portable Secret Key Mechanisms {#smartcards}

Rather than having to transfer secret key material between clients, some users may prefer to rely on portable hardware-backed secret keys in the form of smartcards, USB tokens or other comparable form factors.
These secret keys sometimes require direct user interaction for each use, which can complicate the usability of any MUA that uses them to decrypt a large number of messages.

Guidance on the use of this kind of secret key management are beyond the scope of this document, but future revisions may bring them into scope.

### Active Local Certificate Maintenance {#more-local-cert-maintenance}

{{local-cert-maintenance}} describes conditions where the MUA SHOULD warn the user that something is going wrong with their certificate.

A future version of this document might outline how an MUA could actively avoid these warning situations, for example, by automatically updating the certificate or prompting the user to take specific action.

## Certification Authorities {#cert-authorities}

A future document could offer guidance on how an MUA should select and manage root certification authorities (CAs).

For example:

 - Should the MUA cache intermediate CAs?
 - Should the MUA share such a cache with other PKI clients (e.g., web browsers)?
 - What distinctions are there between a CA for S/MIME and a CA for the web?

## Indexing and Search of Encrypted Messages {#indexing-and-search}

A common use case for MUAs is search of existing messages by keyword or topic.
This is done most efficiently for large mailboxes by assembling an index of message content, rather than by a linear scan of all message content.

When message contents and header fields are encrypted, search by index is complicated.
If the cleartext is not indexed, then messages cannot be found by search.
On the other hand, if the cleartext is indexed, then the index effectively contains the sensitive contents of the message, and needs to be protected.

Detailed guidance on the tradeoff here, including choices about remote search vs local search, are beyond the scope of this document, but a future version of the document may bring them into scope.

## Cached Signature Validation {#cached-sigs}

Asymmetric signature validation can be computationally expensive, and the results can also potentially vary over time (e.g., if a signing certificate is discovered to be revoked).
In some cases, the user may care about the signature validation that they saw when they first read or received the message, not only about the status of the signature verification at the current time.

So, for both performance reasons and historical perspective, it may be useful for an MUA to cache signature validation results in a way that they can be easily retrieved and compared.
Documenting how and when to cache signature validation, as well as how to indicate it to the user, is beyond the scope of this document, but a future version of the document may bring these topics into scope.

## Aggregate Cryptographic Status

This document limits itself to consideration of the cryptographic status of single messages as a baseline concept that can be clearly and simply communicated to the user.
However, some users and some MUAs may find it useful to contemplate even higher-level views of cryptographic status which are not considered directly here.

For example, a future version of the document may also consider how to indicate a simple cryptographic status of message threads (groups of explicitly related messages), conversations (groups of messages with shared sets of participants), peers, or other perspectives that an MUA can provide.

## Expectations of Cryptographic Protection {#expect-e2e}

As mentioned in {{security-indicators}}, the types of security indicators displayed to the user may well vary based on the expectations of the user for a given communication.
At present, there is no widely shared method for the MUA to establish and maintain reasonable expectations about whether a specific received message should have cryptographic protections.

If such a standard is developed, a future version of this document should reference it and encourage the deployment of clearer and simpler security indicators.

## Secure Deletion {#secure-deletion}

One feature many users desire from a secure communications medium is the ability to reliably destroy a message such that it cannot be recovered even by a determined adversary.
In other contexts, a similar desired property is called "forward secrecy".
Doing this with standard e-mail mechanisms like S/MIME and PGP/MIME is challenging because of two interrelated factors:

- A copy of of an e-mail message may be retained by any of the mail transport agents that handle it during delivery, and
- The secret key used to decrypt an encrypted e-mail message is typically retained indefinitely.

This means that an adversary aiming to recover the cleartext contents of a deleted message can do so by getting access to a copy of the encrypted message and the long-term secret key material.

Some mitigation measures may be available to make it possible to delete some encrypted messages securely, but this document considers this use case out of scope.
A future version of the document may elaborate on secure message deleton in more detail.

## Interaction with Opportunistic Encryption

This document focuses on guidance for strong, user-comprehensible end-to-end cryptographic protections for e-mail.
Other approaches are possible, including various forms of opportunistic and transport encryption, which are out of scope for this document.

A future version of this document could describe the interaction between this guidance and more opportunistic forms of encryption, for example some of the scenarios contemplated in {{I-D.dkg-mail-cleartext-copy}}.

## Split Attachments {#split-attachments}

As noted in {{attachments}}, the standard form for encrypted e-mail messages is a single cryptographic envelope.
In a scenario where multiple user agents are drafting a single encrypted message over low-bandwidth links, this can create a poor user experience, as each MUA has to retrieve the full message, including attachments, to modify the draft.
Similarly, when retrieving a message with a large attachment, the receiving MUA might want to only render the main body part, and will have a significant delay in doing so if required to process the full message before handling.

Future work might include an attempt to standardize a mechanism that eases this use case, potentially at the risk of additional metadata leakage about the message (e.g., the size and number of message parts).
Any such work should explicitly try to minimize the risks and concerns described in {{attachments}}.

## Proxy Extensions {#proxy-extensions}

As noted in {{proxy-dangers}}, a proxy-based implementation can be a tempting approach.
But its naive form is likely to be insufficient to provide safe end-to-end encrypted e-mail.

A future version of this document, or a separate but related document, could try to outline the specific additional information, state, and network API surface that would be needed to allow an MUA to be safely integrated with an encryption provider.
Any such work should try to address the potential problems described in {{proxy-dangers}}.

## Mailing Lists {#mailing-lists}

Mailing lists offer challenging complications to any notion of end-to-end cryptographic protections.
By default, there is some sort of intervening MUA (see {{intervening-mua}}), but more than that, user expectations about cryptographic protections might differ from normal messages, at least insofar as they understand they are writing to a mailing list.
A particular challenge to the notion of end-to-end cryptographic security with mailing lists is that a subscriber to a mailing list often does not know who else is subscribed to the mailing list.
Another challenge is that for some mailing lists, some subscribers might not have a valid, non-expired certificate.

Encryption can interact with mailing lists in different ways, depending on the use case of the list.
It's not clear that there are any useful motivations for sending encrypted mail to a publicly archived mailing list.
But an unarchived mailing list might want to provide confidentiality between all recipients, even if the recipients don't know for certain who all the other participants are.
Or, a mailing list with private archives might well decide that two "hops" of encryption (between the sender and the mailing list, and the mailing list and all the subscribers) are a useful confidentiality measure even though they are not "end-to-end" in the sense of the sender directly to all recipients.

Similarly, cryptographic signatures may play different roles in a mailing list, depending on the list's communication goals.
The mailing list itself might want to verify that an incoming message is cryptographically signed by an authorized sender before redistribution to the list subscribers.
It might also want to pass along the sender's signature in a way that the subscribers can all verify it.
Alternately, the mailing list might want to sign each redistributed message itself, and change the message so it appears to come from the list rather than the original sender.

Yet another design for a mailing list with end-to-end cryptographic protections might involve redistributing shared secret keys to all recipients, or using some sort of proxied re-encryption scheme, similar to {{?I-D.wussler-openpgp-forwarding}}.

A future version of this document or a separate but related document might describe some of these tradeoffs and provide guidance for safely meeting common requirements or use cases when combining end-to-end cryptographic protections with mailing lists.

# Document History

## Substantive changes from draft-ietf-...-15 to draft-ietf-...-16

- Address feedback from Last Call and IESG review:
- Add "Weak Encryption" section
- Add Future Work subsection on mailing lists
- Add Future Work subsection on human-readable names
- Mention possible user control over stripping quoted text in cleartext reply to encrypted message
- Simplify Bcc guidance about certificate inclusion

## Substantive changes from draft-ietf-...-14 to draft-ietf-...-15

- update IMAP reference to RFC 9051
- add webmail concerns as part of Future Work

## Substantive changes from draft-ietf-...-13 to draft-ietf-...-14

- Responded to SEC AD Review, including:
- clarify "conformant" vs "legacy" vs "non-cryptographic" MUA categories
- tighten up MUSTs for conformant MUAs
- explicitly recommend encrypting drafts
- clarify debugging as a use case for showing invalid signatures
- clarify "Headers" to "Header Fields"
- 3 states for sending, 4 for receiving

## Substantive changes from draft-ietf-...-12 to draft-ietf-...-13

- clarify RFC 2119 guidance about not rendering cryptographic layers other than message cryptographic status

## Substantive changes from draft-ietf-...-11 to draft-ietf-...-12

- More nuance (and future work) around split attachments
- More nuance (and future work) around proxy-style design
- Clearer caveats about external resources in text/html parts

## Substantive changes from draft-ietf-...-10 to draft-ietf-...-11

- Mention List-* and Archived-At message header fields
- Add additional references to papers that describe flaws in e2e e-mail

## Substantive changes from draft-ietf-...-09 to draft-ietf-...-10

- Introduction: describe one major theme of Future Work

## Substantive changes from draft-ietf-...-08 to draft-ietf-...-09

- Clarify goals of document
- Identify cross-protocol attacks in more detail to justify prescription against key reuse
- Add informative references to Opportunistic Encryption RFC and Certificate Best Practice draft
- Add "Reading Encrypted Messages after Certificate Expiration" section to Common Pitfalls and Guidelines
- Clean up nits identified by Stephen Farrell and Eliot Lear

## Substantive changes from draft-ietf-...-07 to draft-ietf-...-08

- Add guidance about importing and exporting secret key material
- More explanation about "encryption outside, signature inside"
- Guidance about Intervening (resending) MUAs
- Include Sender and Resent-* as user-facing header fields
- Guidance about external subresources for cryptographically protected messages
- Relax "user-facing" definition to be more advisory

## Substantive changes from draft-ietf-...-06 to draft-ietf-...-07

- Add Bernie Hoeneisen and Alexey Melnikov as editors
- Explicitly avoid requiring anything from IANA
- Simplify description of "attachments"
- Add concrete detail on how to compare e-mail addresses
- Explicitly define "Cryptographic Summary"

## Substantive changes from draft-ietf-...-05 to draft-ietf-...-06

- Expand proxy implementation warning to comparable APIs
- Move many marked TODO and FIXME items to "Future Work"
- More detailed guidance on local certificates
- Provide guidance on encrypting draft messages
- More detailed guidanc eon shipping certificates in outbound messages
- Recommend implementing header protection
- Added "Future Work" subsection about interactions with opportunistic approaches

## Substantive changes from draft-ietf-...-04 to draft-ietf-...-05

- Adopt and update text about Bcc from draft-ietf-lamps-header-protection
- Add section about the dangers of an implementation based on a network protocol proxy

## Substantive changes from draft-ietf-...-03 to draft-ietf-...-04

- Added reference to `multipart/oracle` attacks
- Clarified that "Structural Header fields" are the same as RFC2045's "MIME Headers"

## Substantive changes from draft-ietf-...-02 to draft-ietf-...-03

- Added section about mixed recipients
- Noted SMIMEA and OPENPGPKEY DNS RR cert discovery mechanisms
- Added more notes about simplified mental models
- More clarification on one-status-per-message
- Added guidance to defend against EFAIL

## Substantive changes from draft-ietf-...-01 to draft-ietf-...-02

- Added definition of "user-facing" header fields

## Substantive changes from draft-ietf-...-00 to draft-ietf-...-01

- Added section about distinguishing Main Body Parts and Attachments
- Updated document considerations section, including reference to auto-built editor's copy

## Substantive changes from draft-dkg-...-01 to draft-ietf-...-00

- WG adopted draft
- moved Document History and Document Considerations sections to end of appendix, to avoid section renumbering when removed

## Substantive changes from draft-dkg-...-00 to draft-dkg-...-01

- consideration of success/failure indicators for usability
- clarify extendedKeyUsage and keyUsage algorithm-specific details
- initial section on certificate management
- added more TODO items

